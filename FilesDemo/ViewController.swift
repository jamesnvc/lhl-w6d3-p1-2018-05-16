//
//  ViewController.swift
//  FilesDemo
//
//  Created by James Cash on 16-05-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let name = UserDefaults.standard.value(forKey: "name")
        let stuff = UserDefaults.standard.value(forKey: "stuff")
        print("Name: \(name) stuff \(stuff)")

        let docUrl = try! FileManager.default.url(
            for: .documentDirectory,
            in: .userDomainMask,
            appropriateFor: nil, create: false)
        print("Our docs dir is \(docUrl)")
        let fileURL = docUrl.appendingPathComponent("stuffData")
        let data = ["foo", "bar", "baz"]
        do {
            try (data as NSArray).write(to: fileURL)
        } catch let err {
            print("Error writing file: \(err.localizedDescription)")
            abort()
        }

        let readData: [String] = NSArray(contentsOf: fileURL) as! [String]
        print("Read data \(readData)")

        UserDefaults.standard.set("foo", forKey: "name")
        UserDefaults.standard.set([1,3,4], forKey: "stuff")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

